package pl.app.webcrawler.crawler.decision;

import org.jsoup.Jsoup;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TitleKeywordsMarfeelizableDecisionHandlerTest {

    private static final String HTML_TEMPLATE = "<html><head><title>%s</title></head></html>";

    @Test
    public void isMarfeelizableWhenDocumentsTitleContainsOneOfKeywords() throws Exception {
        Set<String> keywords = new HashSet<>(Arrays.asList("news", "noticias"));
        String title = "Greate tech news!!!";

        assertThat(new TitleKeywordsMarfeelizableDecisionHandler(keywords)
                .isMarfeelizable(Jsoup.parse(String.format(HTML_TEMPLATE, title)))
        ).isTrue();

    }


    @Test
    public void isMarfeelizableWhenDocumentsTitleNotContainsAnyKeyword() throws Exception {
        Set<String> keywords = new HashSet<>(Arrays.asList("news", "noticias"));
        String title = "Greate tech conference!!!";

        assertThat(new TitleKeywordsMarfeelizableDecisionHandler(keywords)
                .isMarfeelizable(Jsoup.parse(String.format(HTML_TEMPLATE, title)))
        ).isFalse();

    }

    @Test
    public void isMarfeelizableWhenDocumentsTitleIsEmpty() throws Exception {
        Set<String> keywords = new HashSet<>(Arrays.asList("news", "noticias"));

        assertThat(new TitleKeywordsMarfeelizableDecisionHandler(keywords)
                .isMarfeelizable(Jsoup.parse(String.format(HTML_TEMPLATE, "")))
        ).isFalse();

    }

    @Test
    public void isMarfeelizableWhenDocumentNotContainsTitle() throws Exception {
        Set<String> keywords = new HashSet<>(Arrays.asList("news", "noticias"));

        assertThat(new TitleKeywordsMarfeelizableDecisionHandler(keywords)
                .isMarfeelizable(Jsoup.parse("<html><head></head></html>"))
        ).isFalse();

    }

}