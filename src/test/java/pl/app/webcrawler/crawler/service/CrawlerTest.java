package pl.app.webcrawler.crawler.service;

import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.app.webcrawler.config.CrawlerConfiguration;
import pl.app.webcrawler.crawler.decision.MarfeelizableDecisionHandler;

import java.io.IOException;
import java.net.SocketTimeoutException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CrawlerTest {

    private CrawlerBean service;

    @Mock
    private MarfeelizableDecisionHandler decisionHandler;

    @Mock
    private CrawlerConfiguration.CrawlerProperties crawlerProperties;

    /**
     * After error with connecting to site should return "false".
     *
     * @throws Exception
     */
    @Test
    public void checkSiteWhenBothProtocolsFailsWithTimeout() throws Exception {
        String url = "http://example.com";
        service = new CrawlerBean(decisionHandler, crawlerProperties) {
            @Override
            protected Document getDocument(String protocol, String url) throws IOException {
                throw new SocketTimeoutException();
            }
        };

        assertThat(service.checkSite(url)).isFalse();
    }

    @Test
    public void checkSiteWhenHttpFailsWithTimeout() throws Exception {
        String url = "http://example.com";
        service = new CrawlerBean(decisionHandler, crawlerProperties) {
            @Override
            protected Document getDocument(String protocol, String url) throws IOException {
                if (protocol.equals("http")) {
                    throw new SocketTimeoutException();
                }
                return new Document(url);
            }
        };

        ArgumentCaptor<Document> documentArgumentCaptor = ArgumentCaptor.forClass(Document.class);
        Mockito.when(this.decisionHandler.isMarfeelizable(documentArgumentCaptor.capture())).thenReturn(true);

        assertThat(service.checkSite(url)).isTrue();

    }

    @Test
    public void checkSiteWhenHttpConnects() throws Exception {
        String url = "http://example.com";
        service = new CrawlerBean(decisionHandler, crawlerProperties) {
            @Override
            protected Document getDocument(String protocol, String url) throws IOException {
                if (protocol.equals("http")) {
                    return new Document(url);
                }
                throw new SocketTimeoutException();
            }
        };

        ArgumentCaptor<Document> documentArgumentCaptor = ArgumentCaptor.forClass(Document.class);
        Mockito.when(this.decisionHandler.isMarfeelizable(documentArgumentCaptor.capture())).thenReturn(false);

        assertThat(service.checkSite(url)).isFalse();

    }

}