package pl.app.webcrawler.address.utils;

import org.junit.Test;
import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.dto.AddressResponseDto;
import pl.app.webcrawler.address.model.Address;

import static org.assertj.core.api.Assertions.assertThat;

public class AddressMapperTest {

    @Test
    public void entity2Dto() throws Exception {
        String url = "http://example.com";
        Address address = new Address(url);
        address.setMarfeelizable(true);

        AddressDto dto = AddressMapper.entity2Dto(address, new AddressDto());
        assertThat(dto.getUrl()).isEqualTo(url);

    }

    @Test
    public void entity2ResponseDto() throws Exception {
        String url = "http://example.com";
        Address address = new Address(url);
        address.setMarfeelizable(true);

        AddressResponseDto dto = AddressMapper.entity2Dto(address, new AddressResponseDto());
        assertThat(dto.getUrl()).isEqualTo(url);
        assertThat(dto.getId().toString()).isEqualTo(address.getId().toString());
        assertThat(dto.isMarfeelizable()).isTrue();

    }

}