package pl.app.webcrawler.address.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.model.Address;
import pl.app.webcrawler.address.repository.AddressRepository;
import pl.app.webcrawler.crawler.service.Crawler;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AddressServiceTest {

    @InjectMocks
    private AddressServiceBean service;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private Crawler crawler;

    @Before
    public void setUp() throws Exception {

    }

    /**
     * Url argument is missing.
     *
     * @throws Exception
     */
    @Test
    public void determineUrlWhenNotGiven() throws Exception {

        try {
            service.determineUrl(null);
            assertThat(true).isFalse();
        } catch (IllegalArgumentException e) {
            assertThat(true).isTrue();
        }

        try {
            service.determineUrl(new AddressDto());
            assertThat(true).isFalse();
        } catch (IllegalArgumentException e) {
            assertThat(true).isTrue();
        }

        try {
            service.determineUrl(createAddressDto(""));
            assertThat(true).isFalse();
        } catch (IllegalArgumentException e) {
            assertThat(true).isTrue();
        }

    }

    /**
     * Url not found in database (will be inserted)
     *
     * @throws Exception
     */
    @Test
    public void determineUrlWhenEntityIsNotFound() throws Exception {
        String url = "http://example.com";

        ArgumentCaptor<Address> addressArgumentCaptor = ArgumentCaptor.forClass(Address.class);

        Mockito.when(this.addressRepository.findByUrl(url)).thenReturn(null);
        Mockito.when(crawler.checkSite(url)).thenReturn(false);

        service.determineUrl(createAddressDto(url));

        Mockito.verify(this.addressRepository, Mockito.times(1)).save(addressArgumentCaptor.capture());

        Address persisted = addressArgumentCaptor.getValue();

        assertThat(persisted.getId()).isNotNull();
        assertThat(persisted.getUrl()).isEqualTo(url);
        assertThat(persisted.isMarfeelizable()).isFalse();

    }

    /**
     * Url found in database (will be updated)
     *
     * @throws Exception
     */
    @Test
    public void determineUrlWhenEntityIsFound() throws Exception {
        String url = "http://example.com";
        Address address = new Address(url);
        String id = address.getId().toString();

        assertThat(address.isMarfeelizable()).isFalse();

        Mockito.when(this.addressRepository.findByUrl(url)).thenReturn(address);
        Mockito.when(crawler.checkSite(url)).thenReturn(true);

        service.determineUrl(createAddressDto(url));

        Mockito.verify(this.addressRepository, Mockito.times(1)).save(address);

        assertThat(address.getId().toString()).isEqualTo(id);
        assertThat(address.getUrl()).isEqualTo(url);
        assertThat(address.isMarfeelizable()).isTrue();

    }

    private AddressDto createAddressDto(String url) {
        AddressDto address = new AddressDto();
        address.setUrl(url);

        return address;
    }

}