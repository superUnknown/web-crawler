package pl.app.webcrawler.crawler.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.app.webcrawler.config.CrawlerConfiguration;
import pl.app.webcrawler.crawler.decision.MarfeelizableDecisionHandler;

import java.io.IOException;

@Service
public class CrawlerBean implements Crawler {

    private static final Logger log = LogManager.getLogger(Crawler.class);

    private final MarfeelizableDecisionHandler decisionHandler;

    private final CrawlerConfiguration.CrawlerProperties crawlerProperties;

    @Autowired
    public CrawlerBean(MarfeelizableDecisionHandler decisionHandler, CrawlerConfiguration.CrawlerProperties crawlerProperties) {
        this.decisionHandler = decisionHandler;
        this.crawlerProperties = crawlerProperties;
    }

    @Override
    public boolean checkSite(String url) {
        log.info("Processing url \"{}\".", url);

        Document document;
        try {
            document = getDocument("http", url);
        } catch (IOException e) {
            if (log.isDebugEnabled()) log.debug(e.getMessage(), e);

            try {
                document = getDocument("https", url);
            } catch (IOException e1) {
                log.error("Error occurred during visiting site \"{}\": {}", url, e.getMessage(), e1);
                return false;
            }

        }

        boolean result = this.decisionHandler.isMarfeelizable(document);

        log.info("Url \"{}\" processing success (isMarfeelizable: {}).", url, result);

        return result;

    }

    protected Document getDocument(String protocol, String url) throws IOException {
        return Jsoup.connect(protocol + "://" + url)
                .validateTLSCertificates(this.crawlerProperties.isJsoupValidateTLSCertificates())
                .timeout(this.crawlerProperties.getJsoupTimeoutInMillis())
                .get();
    }

}
