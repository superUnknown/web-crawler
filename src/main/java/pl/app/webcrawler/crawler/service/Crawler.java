package pl.app.webcrawler.crawler.service;

import org.springframework.stereotype.Component;

/**
 * Service responsible for connecting to requested site. Checks is page marfeelizable.
 */
@Component
public interface Crawler {

    boolean checkSite(String url);

}
