package pl.app.webcrawler.crawler.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Logging implementation of failed tasks handler.
 */
public class LoggingRejectedExecutionHandler implements RejectedExecutionHandler {

    private static final Logger log = LogManager.getLogger(LoggingRejectedExecutionHandler.class);

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        log.error("Task {} rejected from {}.", r, executor);
    }
}
