package pl.app.webcrawler.crawler.decision;

import org.jsoup.nodes.Document;

/**
 * Marfeelizable decision handler. Used to define visited site's qualification,
 * which depends on target implementation.
 */
public interface MarfeelizableDecisionHandler {

    /**
     * Should return "true", if document of a site is marfeelizable.
     *
     * @param document
     * @return
     */
    boolean isMarfeelizable(final Document document);

}
