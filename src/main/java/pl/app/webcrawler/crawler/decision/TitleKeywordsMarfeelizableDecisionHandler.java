package pl.app.webcrawler.crawler.decision;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import pl.app.webcrawler.crawler.service.Crawler;

import java.util.Set;

/**
 * Implementation of decision handler. Site is qualified as a marfeelizable
 * if it's title contains given keywords.
 */
public class TitleKeywordsMarfeelizableDecisionHandler implements MarfeelizableDecisionHandler {

    private static final Logger log = LogManager.getLogger(Crawler.class);

    private final Set<String> keywords;

    public TitleKeywordsMarfeelizableDecisionHandler(Set<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public boolean isMarfeelizable(final Document document) {
        String title = document.title();

        if (log.isDebugEnabled())
            log.debug("Checking is site marfeelizable (title contains keywords {} ): {}", this.keywords, title);

        if (title != null && !title.trim().isEmpty()) {
            for (String keyword : this.keywords) {
                if (title.contains(keyword)) {
                    if (log.isDebugEnabled()) log.debug("Site IS marfeelizable. Found match - {} ({})", keyword, title);
                    return true;
                }
            }
        }

        if (log.isDebugEnabled()) log.debug("Site IS NOT marfeelizable. ({})", title);

        return false;
    }

}
