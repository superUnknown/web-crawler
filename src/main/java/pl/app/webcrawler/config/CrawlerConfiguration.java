package pl.app.webcrawler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import pl.app.webcrawler.crawler.decision.MarfeelizableDecisionHandler;
import pl.app.webcrawler.crawler.decision.TitleKeywordsMarfeelizableDecisionHandler;
import pl.app.webcrawler.crawler.utils.LoggingRejectedExecutionHandler;

import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.Executor;

/**
 * Crawler configuration. Configures crawler's executor and marfeelizable decision handler.
 */
@Configuration
@EnableAsync(proxyTargetClass = true)
public class CrawlerConfiguration {

    public static final String CRAWLER_EXECUTOR_BEAN = "CRAWLER_EXECUTOR";

    private final static String CRAWLER_EXECUTOR_THREAD_PREFIX = "crawler_thread-";

    @Bean
    public CrawlerConfiguration.CrawlerProperties crawlerProperties(Environment env) {
        CrawlerConfiguration.CrawlerProperties properties = new CrawlerConfiguration.CrawlerProperties();
        properties.setDecisionHandler(CrawlerConfiguration.CrawlerProperties.DecisionHandler.get(env.getProperty("crawler.decision_handler")));
        properties.setJsoupTimeoutInMillis(env.getProperty("crawler.jsoup_timeout_in_millis", Integer.class));
        properties.setJsoupValidateTLSCertificates(env.getProperty("crawler.jsoup_validate_tls_certificates", Boolean.class));
        properties.getExecutor().setCorePoolSize(env.getProperty("crawler.executor.core_pool_size", Integer.class));
        properties.getExecutor().setMaxPoolSize(env.getProperty("crawler.executor.max_pool_size", Integer.class));
        properties.getExecutor().setQueueCapacity(env.getProperty("crawler.executor.queue_capacity", Integer.class));

        return properties;
    }

    /**
     * Crawler's executor bean configured to process sites concurrently.
     *
     * @param properties
     * @return
     */
    @Bean(name = CRAWLER_EXECUTOR_BEAN)
    public Executor crawlerExecutor(CrawlerProperties properties) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(properties.getExecutor().getCorePoolSize());
        executor.setMaxPoolSize(properties.getExecutor().getMaxPoolSize());
        executor.setQueueCapacity(properties.getExecutor().getQueueCapacity());
        executor.setThreadNamePrefix(CRAWLER_EXECUTOR_THREAD_PREFIX);
        executor.setRejectedExecutionHandler(new LoggingRejectedExecutionHandler());
        executor.initialize();

        return executor;
    }

    /**
     * Marfeelizable decision handler bean. Target implementation is
     * chosen by selected type in application properties.
     *
     * @param properties
     * @return
     */
    @Bean
    public MarfeelizableDecisionHandler marfeelizableDecisionHandler(CrawlerProperties properties) {
        switch (properties.getDecisionHandler()) {
            case TITLE_KEYWORDS:
                return new TitleKeywordsMarfeelizableDecisionHandler(new HashSet<>(Arrays.asList("news", "noticias")));
            default:
                throw new RuntimeException(String.format("Unrecognized marfeelizable decision handler (%s).", properties.getDecisionHandler()));
        }

    }

    /**
     * Stores Crawler's properties. Default values can be override in properties file.
     */
    public static class CrawlerProperties {

        private static final DecisionHandler DEFAULT_DECISION_HANDLER = DecisionHandler.TITLE_KEYWORDS;

        private static final int DEFAULT_JSOUP_TIMEOUT_IN_MILLIS = 1000;

        private static final boolean DEFAULT_JSOUP_VALIDATE_TLS_CERTIFICATES = false;

        private DecisionHandler decisionHandler = DEFAULT_DECISION_HANDLER;

        private Integer jsoupTimeoutInMillis = DEFAULT_JSOUP_TIMEOUT_IN_MILLIS;

        private boolean jsoupValidateTLSCertificates = DEFAULT_JSOUP_VALIDATE_TLS_CERTIFICATES;

        private Executor executor = new Executor();

        public DecisionHandler getDecisionHandler() {
            return decisionHandler;
        }

        public void setDecisionHandler(DecisionHandler decisionHandler) {
            if (decisionHandler == null) {
                return;
            }
            this.decisionHandler = decisionHandler;
        }

        public Integer getJsoupTimeoutInMillis() {
            return jsoupTimeoutInMillis;
        }

        public void setJsoupTimeoutInMillis(Integer jsoupTimeoutInMillis) {
            if (jsoupTimeoutInMillis == null) {
                return;
            }
            this.jsoupTimeoutInMillis = jsoupTimeoutInMillis;
        }

        public boolean isJsoupValidateTLSCertificates() {
            return jsoupValidateTLSCertificates;
        }

        public void setJsoupValidateTLSCertificates(Boolean jsoupValidateTLSCertificates) {
            if (jsoupValidateTLSCertificates == null) {
                return;
            }
            this.jsoupValidateTLSCertificates = jsoupValidateTLSCertificates;
        }

        public Executor getExecutor() {
            return executor;
        }

        public void setExecutor(Executor executor) {
            this.executor = executor;
        }

        public static class Executor {

            private final static int DEFAULT_CORE_POOL_SIZE = 10;

            private final static int DEFAULT_MAX_POOL_SIZE = 10;

            private final static int DEFAULT_QUEUE_CAPACITY = 1000;

            private Integer corePoolSize = DEFAULT_CORE_POOL_SIZE;

            private Integer maxPoolSize = DEFAULT_MAX_POOL_SIZE;

            private Integer queueCapacity = DEFAULT_QUEUE_CAPACITY;

            public Integer getCorePoolSize() {
                return corePoolSize;
            }

            public void setCorePoolSize(Integer corePoolSize) {
                if (corePoolSize == null) {
                    return;
                }
                this.corePoolSize = corePoolSize;
            }

            public Integer getMaxPoolSize() {
                return maxPoolSize;
            }

            public void setMaxPoolSize(Integer maxPoolSize) {
                if (maxPoolSize == null) {
                    return;
                }
                this.maxPoolSize = maxPoolSize;
            }

            public Integer getQueueCapacity() {
                return queueCapacity;
            }

            public void setQueueCapacity(Integer queueCapacity) {
                if (queueCapacity == null) {
                    return;
                }
                this.queueCapacity = queueCapacity;
            }

        }

        /**
         * Enum used to define which marfeelizable decision handler application should use.
         */
        enum DecisionHandler {
            TITLE_KEYWORDS;

            public static DecisionHandler get(String name) {
                if (name == null || name.isEmpty()) {
                    return DEFAULT_DECISION_HANDLER;
                }

                return Arrays.stream(DecisionHandler.values())
                        .filter(d -> d.name().equals(name))
                        .findFirst()
                        .orElse(DEFAULT_DECISION_HANDLER);
            }

        }

    }

}
