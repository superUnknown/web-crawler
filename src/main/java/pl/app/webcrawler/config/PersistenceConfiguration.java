package pl.app.webcrawler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

/**
 * Persistence configuration. Configures in-memory H2 database.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "pl.app.webcrawler")
public class PersistenceConfiguration {

    @Bean
    public PersistenceConfiguration.PersistenceProperties persistenceProperties(Environment env) {
        PersistenceConfiguration.PersistenceProperties properties = new PersistenceConfiguration.PersistenceProperties();
        properties.setGenerateDdl(env.getProperty("persistence.generate_ddl", Boolean.class));
        properties.setShowSql(env.getProperty("persistence.show_sql", Boolean.class));
        properties.setScheme(env.getProperty("persistence.scheme"));

        return properties;
    }

    @Bean
    public Properties jpaProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", PersistenceProperties.HIBERNATE_DIALECT);
        properties.setProperty("hibernate.naming-strategy", PersistenceProperties.HIBERNATE_NAMING_STRATEGY);
        return properties;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter(PersistenceProperties properties) {
        AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setGenerateDdl(properties.isGenerateDdl());
        adapter.setShowSql(properties.isShowSql());
        adapter.setDatabase(Database.H2);
        return adapter;
    }

    @Bean
    public EmbeddedDatabase database(PersistenceProperties properties) {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setName(properties.getScheme())
                .build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(JpaVendorAdapter jpaVendorAdapter, EmbeddedDatabase database, PersistenceProperties properties, Properties jpaProperties) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setPersistenceUnitName(properties.getScheme());
        factory.setPackagesToScan("pl.app.webcrawler");
        factory.setJpaVendorAdapter(jpaVendorAdapter);
        factory.setDataSource(database);
//        factory.setJpaProperties(jpaProperties);
        return factory;
    }


    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    /**
     * Stores persistence properties. Default values can be override in properties file.
     */
    static class PersistenceProperties {

        static final String HIBERNATE_DIALECT = "org.hibernate.dialect.MySQL5InnoDBDialect";

        static final String HIBERNATE_NAMING_STRATEGY = "org.hibernate.cfg.DefaultNamingStrategy";

        private static final boolean DEFAULT_SHOW_SQL = false;

        private static final boolean DEFAULT_GENERATE_DDL = true;

        private static final String DEFAULT_SCHEME = "test";

        private boolean generateDdl = DEFAULT_GENERATE_DDL;

        private boolean showSql = DEFAULT_SHOW_SQL;

        private String scheme = DEFAULT_SCHEME;

        public boolean isGenerateDdl() {
            return generateDdl;
        }

        public void setGenerateDdl(Boolean generateDdl) {
            if (generateDdl == null) {
                return;
            }
            this.generateDdl = generateDdl;
        }

        public boolean isShowSql() {
            return showSql;
        }

        public void setShowSql(Boolean showSql) {
            if (showSql == null) {
                return;
            }
            this.showSql = showSql;
        }

        public String getScheme() {
            return scheme;
        }

        public void setScheme(String scheme) {
            if (scheme == null) {
                return;
            }
            this.scheme = scheme;
        }

    }

}