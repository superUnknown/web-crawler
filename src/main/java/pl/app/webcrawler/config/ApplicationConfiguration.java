package pl.app.webcrawler.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Main application configuration.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "pl.app.webcrawler")
@PropertySource(value = "file:${properties}", ignoreResourceNotFound = true)
public class ApplicationConfiguration {
}