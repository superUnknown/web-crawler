package pl.app.webcrawler.address.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.dto.AddressResponseDto;
import pl.app.webcrawler.address.service.AddressService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AddressCtrlBean implements AddressCtrl {

    private final AddressService addressService;

    @Autowired
    public AddressCtrlBean(AddressService addressService) {
        this.addressService = addressService;
    }

    @Override
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(
            value = "/crawler/determine_urls",
            consumes = "application/json;charset=UTF-8",
            method = RequestMethod.POST
    )
    public void determineUrls(@RequestBody(required = false) List<AddressDto> addresses) {
        Assert.notNull(addresses, "Required addresses are missing (min. 1).");
        Assert.isTrue(!addresses.isEmpty(), "Required addresses are missing (min. 1).");
        addresses.forEach(this.addressService::determineUrl);
    }

    @Override
    @ResponseBody
    @RequestMapping(
            value = "/crawler/urls_marfeelizable_info",
            produces = "application/json;charset=UTF-8",
            method = RequestMethod.GET
    )
    public List<AddressResponseDto> getUrlsMarfeelizableInfo() {
        return this.addressService.getUrlsMarfeelizableInfo();
    }

}
