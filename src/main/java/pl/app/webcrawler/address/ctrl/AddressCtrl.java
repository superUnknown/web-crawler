package pl.app.webcrawler.address.ctrl;

import org.springframework.stereotype.Component;
import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.dto.AddressResponseDto;

import java.util.List;

/**
 * Rest api endpoint to manage url addresses.
 */
@Component
public interface AddressCtrl {

    /**
     * Operation visits given sites to check if they are marfeelizable.
     *
     * @param addresses
     */
    void determineUrls(List<AddressDto> addresses);

    /**
     * Operation returns sites persisted to the database with their marfeelizable qualification.
     *
     * @return
     */
    List<AddressResponseDto> getUrlsMarfeelizableInfo();

}
