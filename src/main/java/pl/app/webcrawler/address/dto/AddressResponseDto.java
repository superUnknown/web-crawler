package pl.app.webcrawler.address.dto;

import java.util.UUID;

/**
 * Extended dto for url address with its id and marfeelizable qualification.
 */
public class AddressResponseDto extends AddressDto {

    private UUID id;

    private boolean marfeelizable;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public boolean isMarfeelizable() {
        return marfeelizable;
    }

    public void setMarfeelizable(boolean marfeelizable) {
        this.marfeelizable = marfeelizable;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AddressResponseDto {");
        sb.append("id=").append(id);
        sb.append(", url=").append(getUrl());
        sb.append(", marfeelizable=").append(marfeelizable);
        sb.append('}');
        return sb.toString();
    }

}
