package pl.app.webcrawler.address.dto;

/**
 * Dto for url address.
 */
public class AddressDto {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AddressDto{");
        sb.append("url='").append(url).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
