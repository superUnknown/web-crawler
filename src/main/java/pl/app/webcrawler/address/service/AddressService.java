package pl.app.webcrawler.address.service;

import org.springframework.stereotype.Component;
import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.dto.AddressResponseDto;

import java.util.List;

/**
 * Service layer for url addresses management.
 */
@Component
public interface AddressService {

    /**
     * Operation visits given site to check if it's marfeelizable. Qualification
     * is handled by MarfeelizableDecisionHandler. Every url is processed in separated
     * thread. Marfeelizable qualification of each site is persisted after successful visit.
     *
     * @param address
     */
    void determineUrl(AddressDto address);

    /**
     * Operation returns sites persisted to the database with their marfeelizable qualification.
     *
     * @return
     */
    List<AddressResponseDto> getUrlsMarfeelizableInfo();

}
