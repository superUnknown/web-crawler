package pl.app.webcrawler.address.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.dto.AddressResponseDto;
import pl.app.webcrawler.address.model.Address;
import pl.app.webcrawler.address.repository.AddressRepository;
import pl.app.webcrawler.address.utils.AddressMapper;
import pl.app.webcrawler.config.CrawlerConfiguration;
import pl.app.webcrawler.crawler.service.Crawler;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressServiceBean implements AddressService {

    private static final Logger log = LogManager.getLogger(AddressService.class);

    private final AddressRepository addressRepository;

    private final Crawler crawler;

    @Autowired
    public AddressServiceBean(AddressRepository addressRepository, Crawler crawler) {
        this.addressRepository = addressRepository;
        this.crawler = crawler;
    }

    @Override
    @Async(CrawlerConfiguration.CRAWLER_EXECUTOR_BEAN)
    public void determineUrl(AddressDto dto) {
        log.info("Determining site marfeelizable: {}", dto);

        Assert.notNull(dto, "Required address is missing.");
        Assert.notNull(dto.getUrl(), "Required address is missing.");
        Assert.isTrue(!dto.getUrl().trim().isEmpty(), "Required address is missing.");

        Address address = this.addressRepository.findByUrl(dto.getUrl());
        if (address == null) {
            address = new Address(dto.getUrl());
        }
        address.setMarfeelizable(this.crawler.checkSite(dto.getUrl()));

        this.addressRepository.save(address);

        log.info("Determining site marfeelizable finished: {}", dto);

    }

    @Override
    public List<AddressResponseDto> getUrlsMarfeelizableInfo() {
        return this.addressRepository.findAll().stream()
                .map(a -> AddressMapper.entity2Dto(a, new AddressResponseDto()))
                .collect(Collectors.toList());
    }

}
