package pl.app.webcrawler.address.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Entity for address information.
 */
@Entity
@Table(name = "Address")
public class Address {

    private UUID id = UUID.randomUUID();

    private String url;

    private boolean marfeelizable;

    public Address() {
    }

    public Address(String url) {
        this.url = url;
    }

    @Id
    @Type(type = "uuid-char")
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isMarfeelizable() {
        return marfeelizable;
    }

    public void setMarfeelizable(boolean marfeelizable) {
        this.marfeelizable = marfeelizable;
    }

}
