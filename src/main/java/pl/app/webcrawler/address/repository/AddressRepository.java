package pl.app.webcrawler.address.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.app.webcrawler.address.model.Address;

/**
 * DAO for url address entity.
 */
public interface AddressRepository extends JpaRepository<Address, Long> {

    /**
     * Gets address entity by url value.
     *
     * @param url
     * @return
     */
    @Query("SELECT a FROM Address a WHERE a.url = ?1")
    Address findByUrl(String url);

}
