package pl.app.webcrawler.address.utils;

import pl.app.webcrawler.address.dto.AddressDto;
import pl.app.webcrawler.address.dto.AddressResponseDto;
import pl.app.webcrawler.address.model.Address;

/**
 * Object mapper that converts entity into dto.
 */
public class AddressMapper {

    /**
     * Converts entity into dto.
     *
     * @param entity
     * @param dto
     * @return
     */
    public static AddressDto entity2Dto(final Address entity, final AddressDto dto) {
        if (entity == null) {
            return null;
        }

        dto.setUrl(entity.getUrl());

        return dto;
    }

    /**
     * Converts entity into dto with extended information.
     *
     * @param entity
     * @param dto
     * @return
     */
    public static AddressResponseDto entity2Dto(final Address entity, final AddressResponseDto dto) {
        if (entity == null) {
            return null;
        }

        entity2Dto(entity, (AddressDto) dto);
        dto.setId(entity.getId());
        dto.setMarfeelizable(entity.isMarfeelizable());

        return dto;
    }

}
