package pl.app.webcrawler;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Main class. Starts the application on Jetty server.
 */
public class Application {

    private static final int PORT = 8080;

    private static final String CONTEXT_PATH = "/";

    private static final String CONFIG_LOCATION = "pl.app.webcrawler.config";

    public static void main(String[] args) throws Exception {
        Server server = new Server(PORT);

        ServletContextHandler handler = new ServletContextHandler();
        server.setHandler(handler);
        handler.setContextPath(CONTEXT_PATH);

        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation(CONFIG_LOCATION);

        ServletHolder holder = new ServletHolder(new DispatcherServlet(context));
        handler.addServlet(holder, CONTEXT_PATH);
        handler.addEventListener(new ContextLoaderListener(context));

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (server.isStarted()) {
                server.setStopAtShutdown(true);
                try {
                    server.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));

        server.start();
        server.join();

    }

}
